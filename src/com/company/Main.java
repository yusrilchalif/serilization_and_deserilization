package com.company;

import java.io.*;

//implement serilization
class Buku implements Serializable
{
    int halaman;

    public Buku(int halaman)
    {
        this.halaman = halaman;
    }
}

//class Catatan doesn't implement serilization
class  Catatan extends Buku
{
    int lembar;

    public Catatan(int lembar, int halaman)
    {
        super(halaman);
        this.lembar = lembar;
    }
}

public class Main {

    public static void main(String[] args)
        throws Exception{
        Catatan catatan = new Catatan(110, 100);

        System.out.println("Lembar = " + catatan.lembar);
        System.out.println("Halaman = " + catatan.halaman);

        //Serilization class Catatan

        //Save to external file
        FileOutputStream fos = new FileOutputStream("hasil.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        //method serilization for catatan class
        oos.writeObject(catatan);

        //close stream
        oos.close();
        fos.close();

        System.out.println("Object Has Been Serilization");

        //Read file
        FileInputStream fis = new FileInputStream("hasil.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);

        //De-serilization method
        Catatan newCatatan = (Catatan)ois.readObject();

        //close stream
        ois.close();
        fis.close();

        System.out.println("\nObject Has Been De-Serilization");

        System.out.println("Lembar = " + newCatatan.lembar);
        System.out.println("Halaman = " + newCatatan.halaman);
    }
}
